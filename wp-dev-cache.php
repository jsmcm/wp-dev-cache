<?php
/*
 * Plugin Name: WP Dev Cache
 * Plugin URI: https://wptachyon.com
 * Description: Theme and plugin developer's caching plugin
 * Version: 1.0.6
 * Author: John McMurray <info@wptachyon.com>
 * Author URI: https://wptachyon.com
*/


/*  Copyright 2019 WPTachyon.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


require_once dirname( __FILE__ )."/cache.php";

class WPDevCache
{

    var $oCache;

    /**
     * Constructor
     *
     * @return null
     */
    public function __construct( $skipWP=false )
    {

	    if ( $skipWP == false ) { // if true it allows us to use caching before WP gets invoked.

            if ( ! function_exists( "plugin_dir_path" ) ) {
                // this might be from a direct call before WP loaded
                return;
            }

        	// Plugin Details
        	$this->plugin = new stdClass;
        	$this->plugin->name = 'wp-dev-cache'; // Plugin Folder
        	$this->plugin->displayName = 'WP Dev Cache'; // Plugin Name
        	$this->plugin->version = '1.0.6';
        	$this->plugin->folder = plugin_dir_path( __FILE__ );
        	$this->plugin->url = plugin_dir_url( __FILE__ );
        }
        
        // Hooks and filters
        //add_action( 'init', array( &$this, "initCache" ) );


        // FOR NOW, DISABLE MEMCACHED AS ITS PUTTING JSON OUT ON THE SCREEN
        //try {
        //    $this->oCache = new \WPTachyon\WPDevCache\Cache( "memcached" );
        ///} catch ( Exception $e ) {
        //    if ( $e->getMessage() == "memcached failed" ) {
                $this->oCache = new \WPTachyon\WPDevCache\Cache( "file" );
        //    }
        //}
    
        
        add_action('admin_bar_menu', array( &$this, 'toolBarMenu' ), 100);
        add_action('admin_menu', array(&$this, 'addPages'));
        
        if ( ! file_exists( $_SERVER["DOCUMENT_ROOT"]."/wp-dev-cache.nginx.conf" ) ) {
            
            
            $nginx = "";
            $nginx = $nginx."###############################\r\n"; 
            $nginx = $nginx."#\r\n";
            $nginx = $nginx."# Check for brotli compression\r\n"; 
            $nginx = $nginx."#\r\n";
            $nginx = $nginx."###############################\r\n"; 
            $nginx = $nginx."set \$compression \"na\";\r\n";
            $nginx = $nginx."if (\$http_accept_encoding ~ br) {\r\n"; 
                $nginx = $nginx."    set \$compression \".br\";\r\n"; 
                $nginx = $nginx."}\r\n";
                $nginx = $nginx."\r\n";
            $nginx = $nginx."if (-f \"\$document_root/wp-content/cache/wp-dev-cache/page/\$request_uri/index.html\$compression\") {\r\n";
                $nginx = $nginx."    rewrite .* \"/wp-content/cache/wp-dev-cache/page/\$request_uri/index.html\$compression\" last;\r\n";
            $nginx = $nginx."}\r\n"; 
            $nginx = $nginx."\r\n";
            $nginx = $nginx."location ~ /wp-content/cache/wp-dev-cache/page.*br\$ {\r\n"; 
            $nginx = $nginx."    gzip off;\r\n";
            $nginx = $nginx."    types {}\r\n";
            $nginx = $nginx."    default_type text/html;\r\n";
            $nginx = $nginx."    add_header X-Powered-By \"WP Dev Cache - Brotli\";\r\n"; 
            $nginx = $nginx."    add_header Vary \"Accept-Encoding, Cookie\";\r\n"; 
            $nginx = $nginx."    add_header Pragma \"public\";\r\n";
            $nginx = $nginx."    add_header Cache-Control \"public\";\r\n";
            $nginx = $nginx."    add_header Content-Encoding br;\r\n"; 
            $nginx = $nginx."}\r\n"; 
            $nginx = $nginx."\r\n";

            $nginx = $nginx."###############################\r\n"; 
            $nginx = $nginx."#\r\n"; 
            $nginx = $nginx."# Check for gzip compression\r\n"; 
            $nginx = $nginx."#\r\n"; 
            $nginx = $nginx."###############################\r\n";
            $nginx = $nginx."set \$compression \"na\";\r\n"; 
            $nginx = $nginx."if (\$http_accept_encoding ~ gzip) {\r\n"; 
                $nginx = $nginx."    set \$compression \".gzip\";\r\n"; 
                $nginx = $nginx."}\r\n"; 
            $nginx = $nginx."if (-f \"\$document_root/wp-content/cache/wp-dev-cache/page/\$request_uri/index.html\$compression\") {\r\n"; 
                $nginx = $nginx."    rewrite .* \"/wp-content/cache/wp-dev-cache/page/\$request_uri/index.html\$compression\" last;\r\n"; 
            $nginx = $nginx."}\r\n";
            $nginx = $nginx."location ~ /wp-content/cache/wp-dev-cache/page.*gzip\$ {\r\n"; 
            $nginx = $nginx."    gzip off;\r\n";
            $nginx = $nginx."    types {}\r\n";
            $nginx = $nginx."    default_type text/html;\r\n";
            $nginx = $nginx."    add_header X-Powered-By \"WP Dev Cache - GZIP\";\r\n"; 
            $nginx = $nginx."    add_header Vary \"Accept-Encoding, Cookie\";\r\n"; 
            $nginx = $nginx."    add_header Pragma \"public\";\r\n"; 
            $nginx = $nginx."    add_header Cache-Control \"public\";\r\n"; 
            $nginx = $nginx."    add_header Content-Encoding gzip;\r\n"; 
            $nginx = $nginx."}\r\n"; 
            $nginx = $nginx."\r\n";

            $nginx = $nginx."###############################\r\n";
            $nginx = $nginx."#\r\n";
            $nginx = $nginx."# Check for uncompressed cache\r\n";
            $nginx = $nginx."#\r\n";
            $nginx = $nginx."###############################\r\n";
            $nginx = $nginx."if (-f \"\$document_root/wp-content/cache/wp-dev-cache/page/\$request_uri/index.html\") {\r\n"; 
                $nginx = $nginx."    rewrite .* \"/wp-content/cache/wp-dev-cache/page/\$request_uri/index.html\" last;\r\n";
            $nginx = $nginx."}\r\n";
            $nginx = $nginx."location ~ /wp-content/cache/wp-dev-cache/page.*\$ {\r\n"; 
            $nginx = $nginx."    gzip off;\r\n";
            $nginx = $nginx."    types {}\r\n";
            $nginx = $nginx."    default_type text/html;\r\n";
            $nginx = $nginx."    add_header X-Powered-By \"WP Dev Cache - NO COMPRESSION\";\r\n"; 
            $nginx = $nginx."    add_header Vary \"Accept-Encoding, Cookie\";\r\n"; 
            $nginx = $nginx."    add_header Pragma \"public\";\r\n";
            $nginx = $nginx."    add_header Cache-Control \"public\";\r\n"; 
            $nginx = $nginx."}\r\n";

            file_put_contents(  $_SERVER["DOCUMENT_ROOT"]."/wp-dev-cache.nginx.conf", $nginx );

        }
    }



    function purgeCache()
    {
        
        $this->clear( );
        
        $referer = "";
        if ( isset( $_SERVER["HTTP_REFERER"] ) ) {
            $referer = esc_url( $_SERVER["HTTP_REFERER"] );
        }

        print "<div class=\"success\"><p>Cache Purged!</p>";
        

        if ( $referer != "" && !strstr($referer, "?page=wp-dev-cache-purge")) {
            print "<br><a href=\"".$referer."\">Go back to where you came from</a>";
        }    
        
        
        print "</p></div>";
        
    }


    function addPages()
    {

        add_submenu_page( 
            null            // -> Set to null - will hide menu link
          , 'WP Dev Cache'    // -> Page Title
          , 'WP Dev Cache'    // -> Title that would otherwise appear in the menu
          , 'manage_options' // -> Capability level
          , 'wp-dev-cache-purge'   // -> Still accessible via admin.php?page=wp-dev-cache-purge
          , array( &$this, 'purgeCache') // -> To render the page
        );

    }

    function toolBarMenu( $admin_bar )
    {

        $admin_bar->add_menu( array(
            'id'    => 'wp-dev-cache',
            'title' => 'WP Dev Cache',
            'href'  => '#',
            'meta'  => array(
                'title' => __('WP Dev Cache'),            
            ),
        ));

        $admin_bar->add_menu( array(
            'id'    => 'wp-dev-cache-purge-cache',
            'parent' => 'wp-dev-cache',
            'title' => 'Purge Cache',
            'href'  => '/wp-admin/admin.php?page=wp-dev-cache-purge',
            'meta'  => array(
                'title' => __('Purge Cache'),
                'target' => '_blank',
                'class' => 'my_menu_item_class'
            ),
        ));
    }


    function clearCache()
    {
        $this->clear( );
    }



    public function get( $key, $default=null, $skipIfLoggedIn=true )
    {       
        if ( is_user_logged_in() && $skipIfLoggedIn == true ) {
            return $default;
        } else if ( is_user_logged_in() && $skipIfLoggedIn == false ) {
            $userId = get_current_user_id();
            $key = "uid_".$userId."_".$key;
        }

        return $this->oCache->get( $key, $default );
    }

    public function set( $key, $contents, $ttl=null, $skipIfLoggedIn=true )
    {  
        if ( is_user_logged_in() && $skipIfLoggedIn == true ) {
            return false;
        } else if ( is_user_logged_in() && $skipIfLoggedIn == false ) {
            $userId = get_current_user_id();
            $key = "uid_".$userId."_".$key;
        }

        return $this->oCache->set( $key, $contents, $ttl );
    }

    public function delete( $key )
    {
        return $this->oCache->delete( $key );
    }
 
    public function clear()
    {
        return $this->oCache->clear(  );
    }

}

global $oWPDevCache;
$oWPDevCache = new WPDevCache();
