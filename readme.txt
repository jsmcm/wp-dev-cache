=== WP Dev Cache ===
Contributors: wptachyon.com
Tags: cache, memcached, redis, file cache, page cache, function cache
Requires at least: 5.0
Tested up to: 5.0
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 

== Description ==

A caching plugins to use a variety of caching drivers such as file based, memecached or redis.

Its used by theme and plugin developers to cache parts of pages. It could be used as a page cache but Its primary function is caching page fragments. Eg, if your plugin populates a table which is database and processing intensive then you can wrap that function in a cache call to save resources on subsequent page loads.

== Example Usage ==

    The example below shows how to cache a section of page template to get the title bar.

    <?php

    $ID = get_the_ID();

    $cache = $oWPDevCache->get( "title_container_".$ID, false ); // use the ID as part of the cache name to make a per page cache

    if ( $cache === false ) {
        
        // Nothing in the cache, get it and cache it
        $thumbNail = "";
        $background =  "";

        if ( has_post_thumbnail( $ID ) ) {

            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'full' );
            $thumbNail = $thumb['0'];
            
            $background = "url( ".$thumbNail." ); ";
        }
        
        $cache = "<div class=\"title-container\" style=\"background-image: ".$background.";\">";
                    $cache = $cache."<h1> ".get_the_title()." </h1>";
        $cache = $cache ."</div>";

        $oWPDevCache->set( "title_container_".$ID, $cache, 3600 ); // expire in an hour
    }

    print $cache



== TODO ==

In the next iteration we'll be adding basic management such as a clear cache button as well as an enable / disable cache toggle.

We also require a method to select between different cache types (redis, memecached, file based) as this is hard coded at the moment

 
== Changelog ==
= 1.0.7 =
* Added redis to options

= 1.0.6 =
* Added memcached back in after fixing json output bug

= 1.0.2 =
* Adds a purge cache WP admin toolbar menu item
* Adds comment to bottom of page if page gzip or brotli compressed and server by nginx directly

= 1.0.1 = 
* Initial public release

= 1.0.0 =
* Development version.
