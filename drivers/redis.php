<?php

namespace WPTachyon\WPDevCache;

require_once dirname( __FILE__ )."/DriverInterface.php";

use \WPTachyon\WPDevCache\DriverInterface;

$keyPrepend = "";

class Redis implements DriverInterface
{
    var $redisServer = null;
    

    /**
     * Constructor class. 
     * Optional parameters of IP and port
     * 
     * @param string $hostIp   The IP address to redis
     * @param string $hostPort The port
     * @return null
     */    
    public function __construct( $hostIp="", $hostPort="", $keyPrepend="" )
    {

        $this->keyPrepend = $keyPrepend;

        if( $hostIp != "" ) {

            if ( ! filter_var( $hostIp, FILTER_VALIDATE_IP ) ) {
                throw new \Exception( $hostIp." is not a valid IP address");
            }

        } else {
            $hostIp = "127.0.0.1";
        }


        if( $hostPort != "" ) {

            if ( ! is_numeric( $hostPort ) ) {
                throw new \Exception( $hostPort." is not a valid port number");
            }

        } else {
            $hostPort = 6379;            
        }
        

        if ( class_exists("\Redis") ) {
            // test cache
            try {
                $this->redisServer = new \Redis();

                $result = $this->redisServer->connect( $hostIp, $hostPort );
                

                $testKey = "wp-dev-cache-".rand( 0, 100000 ).md5( microtime( ) );
                $testValue = "wp-dev-cache-".rand( 0, 100000 ).md5( microtime( ) );

                $this->redisServer->setex( $testKey, 2, $testValue);
                if ( $this->redisServer->get( $testKey ) != $testValue ) {
                    throw new \Exception( "redis failed at test 1" );
                }

                $this->redisServer->del( $testKey );

            } catch (\Exception $e) {
                throw new \Exception( "redis failed: ".$e->getMessage() );
            }
        } else {
            throw new \Exception( "redis does not exists!" );
        }
    }



    public function get( $key, $default = null )
    {

        if ( ! ( $value = $this->redisServer->get( $this->keyPrepend.$key ) ) ) {
            return $default;
        }

        $contents = json_decode( $value, true );

        if ( ( ! isset( $contents["ttl"] ) ) || ( ! isset( $contents["value"] ) ) ) {
            $this->redisServer->del( $this->keyPrepend.$key );
            return $default;
        }

        return  unserialize( $contents["value"] );

    }


    function set( $key, $contents )
    { 
        $this->redisServer->setex( $this->keyPrepend.$key, $contents["ttl"], json_encode( $contents ) );
    }
    

    function delete( $key )
    {
        $this->redisServer->del( $this->keyPrepend.$key );

        return true;        
    }


    function clear()
    {

        $this->redisServer->flushAll();

    }    

}