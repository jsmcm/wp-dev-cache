<?php

namespace WPTachyon\WPDevCache;

require_once dirname( __FILE__ )."/DriverInterface.php";

use \WPTachyon\WPDevCache\DriverInterface;

class File implements DriverInterface
{
    var $filePath = "";

    /**
     * Constructor class. 
     * Optional parameter of file path
     * 
     * @param string $filePath The path to the cache directory
     * @return null
     */    
    public function __construct( $filePath="" )
    {
        clearstatcache();

        if ( $filePath == "" ) {
            $filePath = WP_CONTENT_DIR."/cache/wp-dev-cache";
        }

        if ( ! file_exists( $filePath."/page" ) ) {
            @mkdir( $filePath."/page", 0755, true );
        }             

        if ( ! file_exists( $filePath."/page" ) ) {
            throw new Exception( "File path (".$filePath.") does not exist or could not be created!");
        }

        $this->filePath = $filePath;

    }



    public function get( $key, $default = null )
    {
        $filePath = $this->filePath."/";

        if ( strstr( $key, "/" ) ) {
            $filePath = $filePath."page";
        }

        $fileName = $filePath.$key;
        if ( is_dir( $filePath.$key ) ) {
            if ( substr($key, strlen($key) - 1, 1) !== "/" ) {
                $key = $key."/";
            }

            $fileName = $filePath.$key."json";
        }
         
        if ( ! file_exists( $fileName ) ) {
            return $default;
        }


        $contents = json_decode( file_get_contents( $fileName ), true );
        
        if ( ( ! isset( $contents["ttl"] ) ) || ( ! isset( $contents["value"] ) ) ) {
            $this->delete( $key );
            return $default;
        }

	if ( $contents["ttl"] != "0" ) { // zero means don't expire {
            if( ( time() - filemtime( $fileName ) ) > intVal( $contents["ttl"] ) ) {
                // Cache expired
                $this->delete( $key );
                return $default;            
            }
	}

        return unserialize( $contents["value"] );

    }

    function set( $key, $contents )
    {
        $filePath = $this->filePath."/";

        if ( strstr( $key, "/" ) ) {
            $filePath = $filePath."page";
       
            if ( !file_exists( $filePath.$key ) ) {
                mkdir( $filePath.$key, 0755, true );
            }

            // save the full page
            file_put_contents( $filePath.$key."/index.html", $contents["value"] );

            if ( function_exists( "gzencode" ) ) {
                $gzdata = gzencode($contents["value"]."\r\n<!-- gzip compressed by wp-dev-cache -->", 9);
                file_put_contents( $filePath.$key."/index.html.gzip", $gzdata );
            }

            //if ( function_exists( "brotli_compress" ) ) {
                //$gzdata = brotli_compress($contents["value"]."\r\n<!-- Brotli compressed by wp-dev-cache -->", 11);
                //file_put_contents( $filePath.$key."/index.html.br", $gzdata );  
            //}

            $key = $key."/json"; // set here so that we can keep the ttl info
        }


        file_put_contents( $filePath.$key, json_encode( $contents ) );
    }


    function delete( $key )
    {

        $filePath = $this->filePath."/";

        if ( strstr( $key, "/" ) ) {
            $filePath = $filePath."page";
            
        }


        clearstatcache();

        if ( file_exists( $filePath.$key ) && is_file( $filePath.$key ) ) {
            @unlink( $filePath.$key );
        }

        if ( file_exists( $filePath.$key."/json" ) ) {
            @unlink( $filePath.$key."/json" );
        }

        if ( file_exists( $filePath.$key."/index.html" ) ) {
            @unlink( $filePath.$key."/index.html" );
        }

        if ( file_exists( $filePath.$key."/index.html.gzip" ) ) {
            @unlink( $filePath.$key."/index.html.gzip" );
        }

        if ( file_exists( $filePath.$key."/index.html.br" ) ) {
            @unlink( $filePath.$key."/index.html.br" );
        }

        if ( file_exists( $filePath.$key ) && is_dir( $filePath.$key ) ) {
            $this->deleteDirectory( $filePath.$key );
        }

        if ( ! file_exists( $filePath.$key ) ) {
            return true;
        }

        return false;        
    }

    function clear()
    {

  
        $dh = opendir($this->filePath);

        while ( ( $file = readdir( $dh ) ) !== false ) {
            if ( $file != '.' && $file != '..' ) {
                $this->delete($file);    
            }
        }

        closedir($dh);


    
    }

    private function deleteDirectory($directoryName)
    {

            if( file_exists($directoryName) ) {   
            if( $this->deleteDirectoryRecursive($directoryName) ) {
                return true;
            }
            }
        
        return false;
    }


    private function deleteDirectoryRecursive($dir)
    {
        clearstatcache();

       
            if (!file_exists($dir)) {
                    return true;
            }

            if (!is_dir($dir) || is_link($dir)) {
                    return unlink($dir);
            }

            foreach (scandir($dir) as $item) {
                    if ($item == '.' || $item == '..') {
                            continue;
                    }

                    if (!$this->deleteDirectoryRecursive($dir . "/" . $item)) {
                            chmod($dir . "/" . $item, 0777);

                            if (!$this->deleteDirectoryRecursive($dir . "/" . $item)) {
                                    return false;
                            }
                    }
            }

            if (file_exists($dir)) {
                return @rmdir($dir);
            }

            return true;
    }


}
