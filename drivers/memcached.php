<?php

namespace WPTachyon\WPDevCache;

require_once dirname( __FILE__ )."/DriverInterface.php";

use \WPTachyon\WPDevCache\DriverInterface;

class Memcached implements DriverInterface
{
    var $memcachedServer = null;
    

    /**
     * Constructor class. 
     * Optional parameters of IP and port
     * 
     * @param string $hostIp   The IP address to memcached
     * @param string $hostPort The port
     * @return null
     */    
    public function __construct( $hostIp="", $hostPort="" )
    {
        if( $hostIp != "" ) {

            if ( ! filter_var( $hostIp, FILTER_VALIDATE_IP ) ) {
                throw new \Exception( $hostIp." is not a valid IP address");
            }

        } else {
            $hostIp = "127.0.0.1";
        }


        if( $hostPort != "" ) {

            if ( ! is_numeric( $hostPort ) ) {
                throw new \Exception( $hostPort." is not a valid port number");
            }

        } else {
            $hostPort = 11211;            
        }
        

        if ( class_exists("\Memcached") ) {
            // test cache
            try {
                $this->memcachedServer = new \Memcached();

                $result = $this->memcachedServer->addServer( $hostIp, $hostPort );

                $testKey = "wp-dev-cache-".rand( 0, 100000 ).md5( microtime( ) );
                $testValue = "wp-dev-cache-".rand( 0, 100000 ).md5( microtime( ) );

                $this->memcachedServer->add( $testKey, $testValue, 1 );
                if ( $this->memcachedServer->get( $testKey ) != $testValue ) {
                    throw new \Exception( "memcached failed" );
                }

                $this->memcachedServer->delete( $testKey );
            } catch (\Exception $e) {
                throw new \Exception( "memcached failed" );
            }
        } else {
            throw new \Exception( "memcached does not exists!" );
        }
    }



    public function get( $key, $default = null )
    {

        if ( ! ( $value = $this->memcachedServer->get( $key ) ) ) {
            return $default;
        }

        $contents = json_decode( $value, true );

        if ( ( ! isset( $contents["ttl"] ) ) || ( ! isset( $contents["value"] ) ) ) {
            $this->memcachedServer->delete( $key );
            return $default;
        }

        return  unserialize( $contents["value"] );

    }


    function set( $key, $contents )
    { 
        $this->memcachedServer->set( $key , json_encode( $contents ), $contents["ttl"] );
    }
    

    function delete( $key )
    {
        $this->memcachedServer->delete( $key );

        return true;        
    }


    function clear()
    {

        $this->memcachedServer->flush();

    }    

}