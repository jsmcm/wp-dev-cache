<?php

namespace WPTachyon\WPDevCache;

interface DriverInterface
{
    /**
     * Fetches a value from the cache.
     *
     * @param string $key     The unique key of this item in the cache.
     * @param mixed  $default Default value to return if the key does not exist.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     *
     */
    public function get( $key, $default = null );


    /**
     * Persists data in the cache, uniquely referenced by a key
     *
     * @param string                 $key      The key of the item to store.
     * @param mixed                  $contents The json string making up value and ttl
     *
     * @return bool True on success and false on failure.
     * 
     */
    public function set($key, $contents);


    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     *
     */
    public function delete($key);    

        /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear();


}
