<?php

namespace WPTachyon\WPDevCache;

require_once dirname( __FILE__ )."/psr/cache/CacheInterface.php";

use \Psr\SimpleCache\CacheInterface;

class Cache implements CacheInterface
{

    var $oDriver = null;

    /**
     * Constructor class. 
     * Takes zero or more paremeters to specify setup.
     * First argument must be type of parameter followed by additional info.
     * Type are file, memcached (redis to follow soon).
     * 
     * File based: 
     * [file, [path]]
     * 
     * Memcached:
     * 'memcached'[, 'ip address'[, port]]
     * 
     * @param ...string $args 0 or more parameters
     * @return null
     */
    public function __construct( ...$args ) 
    {

        $allowedDrivers = [
            "file",
            "memcached",
            "redis"
        ];

        $driver = "file";

        $argCount = count( $args );
        if ( $argCount > 0 ) {
            $args[0] = strtolower( $args[0] );

            if ( ! in_array($args[0], $allowedDrivers) ) {
                throw new \Exception( "Cache driver incorrect. Valid cache drivers are ". implode(" and ", $allowedDrivers).". You said '".$args[0]."'" );
            }

            $driver = $args[0];
        }


        if ( $driver == "memcached" ) { 

            require_once dirname( __FILE__ )."/drivers/memcached.php";

            /*
            arg[1] => $hostIp
            arg[2] => $hostPort
            */

            try {
                if ( $argCount == 1 ) {

                    $this->oDriver = new \WPTachyon\WPDevCache\Memcached( );

                } else if ( $argCount == 2 ) {

                    $this->oDriver = new \WPTachyon\WPDevCache\Memcached( $args[1] );

                } else if ( $argCount == 3 ) {

                    $this->oDriver = new \WPTachyon\WPDevCache\Memcached( $args[1], $args[2] );

                }  
            } catch ( \Exception $e ) {
                
                if( $e->getMessage() == "memcached does not exists, downgrading to file driver!" ) {
                    $driver = "file";
                } else {
                    throw new \Exception( $e->getMessage() );
                }
            }
        }

        

        if ( $driver == "redis" ) { 

            require_once dirname( __FILE__ )."/drivers/redis.php";

            /*
            arg[1] => $hostIp
            arg[2] => $hostPort
            arg[3] => $keyPrepend
            */

            try {
                if ( $argCount == 1 ) {

                    $this->oDriver = new \WPTachyon\WPDevCache\Redis( );

                } else if ( $argCount == 2 ) {

                    $this->oDriver = new \WPTachyon\WPDevCache\Redis( $args[1] );

                } else if ( $argCount == 3 ) {

                    $this->oDriver = new \WPTachyon\WPDevCache\Redis( $args[1], $args[2] );

                } else if ( $argCount == 4 ) {

                    $this->oDriver = new \WPTachyon\WPDevCache\Redis( $args[1], $args[2], $args[3] );

                }  

            } catch ( \Exception $e ) {
                
                if( $e->getMessage() == "redis does not exists, downgrading to file driver!" ) {
                    $driver = "file";
                } else {
                    throw new \Exception( $e->getMessage() );
                }

            }
        }

        
        if ( $driver == "file" ) { 

            require_once dirname( __FILE__ )."/drivers/file.php"; 

            if( $argCount > 1 ) {

                $this->oDriver = new \WPTachyon\WPDevCache\File( $args[1] );

            } else {

                $this->oDriver = new \WPTachyon\WPDevCache\File( );

            }

        } 
        
        
    
    }


    /**
     * Checks if the variable is a valid file name
     * 
     * @param string $name The name
     * 
     * @return bool
     */
    private function isValidName( $name ) 
    {
	    return true;
        return preg_match('/^[\/]?([-\.\w]*)[\/]?$/', $name) > 0;
    }
    
    
    public function has ( $key )
    {
        $random = md5( microtime() );

        if ( $this->get( $key, $random ) == $random ) {
            return false;
        }

        return true;
    }

    public function get( $key, $default = null )
    {
        if ( $this->isValidName( $key ) === false ) {
            // this should throw an invalid argument exception type
            // but for this simple class we're just throwing a 
            // regular exception
            throw new \Exception( "Invalid key. Please use a key name (no spaces or special characters" );
        }

        return $this->oDriver->get( $key, $default );
    }


    public function set( $key, $value, $ttl = null )
    {

        if ( $this->isValidName( $key ) === false ) {
            // this should throw an invalid argument exception type
            // but for this simple class we're just throwing a 
            // regular exception
            throw new \Exception( "Invalid key. Please use a key which is also a valid file name (no spaces or special characters" );
        }


        if ( ( $ttl === null ) || ( ! is_numeric( $ttl ) ) || ( $ttl < 0 ) ) {
            $ttl = 1800; // 30 mins
        }

        
        $contents = ["value" => serialize( $value ), "ttl" => $ttl];
    
        $this->oDriver->set( $key, $contents );
        
        return true;

    }

    public function delete( $key )
    {
        if ( $this->isValidName( $key ) === false ) {
            // this should throw an invalid argument exception type
            // but for this simple class we're just throwing a 
            // regular exception
            throw new \Exception( "Invalid key. Please use a key which is also a valid file name (no spaces or special characters" );
        }

        return $this->oDriver->delete( $key );
        
    }



    public function clear()
    {
        $this->oDriver->clear();
        return true;
    }


    public function getMultiple($keys, $default = null)
    {
        return false;
    }
    public function setMultiple($values, $ttl = null)
    {
        return false;
    }
    public function deleteMultiple($keys)
    {
        return false;
    }

}

